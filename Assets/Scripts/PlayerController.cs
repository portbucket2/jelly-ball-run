﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] ControlType controlType;
    [SerializeField] bool inputEnabled = true;
    [SerializeField] bool isMoving = true;
    [SerializeField] public bool isBouncing = true;
    [SerializeField] GameObject jelloPrefab;
    [SerializeField] float maxJelloSize = 1;
    [SerializeField] float currentJelloSize = 1;
    [SerializeField] int maxJellos = 4;
    [SerializeField] float transitionTime = 0.5f;
    [SerializeField] float moveSpeed = 1f;
    [SerializeField] float dragGap = 10f;
    [SerializeField] LayerMask wallLayer;

    [Header("Separated Positions")]
    [SerializeField] float gap = 0.5f;
    [SerializeField] List<Vector3> jelloPositions;

    [Header("Debug values")]
    [SerializeField] int currentJelloCount = 1;
    [SerializeField] int oldJelloCount = 1;
    [SerializeField] int jelloLife = 10;
    [SerializeField] float currentMass = 1f;
    [SerializeField] float shedMassPerc = 0.2f;
    [SerializeField] Obstacle currentObstacle;
    [SerializeField] UIController getUI;
    [SerializeField] Button increaseButton;

    [SerializeField] List<Jello> jellos;
    [SerializeField] List<Material> jellosMats;

    float holdCount = 0;
    Vector3 mousePosition;
    Vector3 jelloStartPos;
    readonly string OBSTACLE = "obstacle";
    readonly string FINISHLINE = "finishLine";
    readonly string DISTURBANCE = "_disturbance";

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    float dragDistance = 0f;
    int dragDistanceCount = 0;

    void Start()
    {
        increaseButton.onClick.AddListener(delegate {
            //SeparateTwoJellos(0, 1, transitionTime);
	    });

        jelloStartPos = jelloPrefab.transform.position;
        jellos = new List<Jello>();
        jellosMats = new List<Material>();
        jellos.Add(jelloPrefab.GetComponent<Jello>());
        jellosMats.Add(jelloPrefab.GetComponent<MeshRenderer>().material);
        for (int i = 0; i < maxJellos - 1; i++) 
        {
            GameObject gg = Instantiate(jelloPrefab, transform);
            jellos.Add(gg.transform.GetComponent<Jello>());
            jellosMats.Add(gg.transform.GetComponent<MeshRenderer>().material);
        }
        getUI.SetLifeSlider(jelloLife);

        Invoke("AtStart", 0.2f);
    }
    void AtStart()
    {
        startMoving(controlType);
    }

    // Update is called once per frame
    void Update()
    {
        if (inputEnabled)
        {
            if (controlType == ControlType.SWIPE)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    mousePosition = Input.mousePosition;
                }
                if (Input.GetMouseButtonUp(0))
                {
                    if (Input.mousePosition.y > mousePosition.y)
                    {
                        //swiped up
                        SwipedUp();
                    }
                    else
                    {
                        //swiped down
                        SwipedDown();
                    }
                }
            }
            else
            {//DRAG
                if (Input.GetMouseButtonDown(0))
                {
                    holdCount = 0;
                    mousePosition = Input.mousePosition;
                }
                if (Input.GetMouseButton(0))
                {
                    holdCount += Time.deltaTime;
                    dragDistance = Vector3.Distance(Input.mousePosition, mousePosition);
                    //dragDistance = dragDistance / dragGap;
                    //dragDistanceCount = Mathf.FloorToInt(dragDistance);
                    //Debug.LogError("drag count = " + dragDistanceCount);
                    if (dragDistance > dragGap)
                    {

                        //currentJelloCount = dragDistanceCount;
                        //if (currentJelloCount > 0)
                        //{
                        //    SetJellosOrder();
                        //    WallCheck();
                        //}
                        //swiped up
                        if (Input.mousePosition.y > mousePosition.y)
                        {
                            // increase
                            SwipedUp();
                        }
                        else//swiped down
                        {
                            // decrease
                            SwipedDown();
                        }
                        mousePosition = Input.mousePosition;
                    }
                }
                if (Input.GetMouseButtonUp(0))
                {
                    //holdCount = 0;
                    //currentJelloCount = 1;
                    //SetJellosOrder();

                    //WallCheck();
                }
            }
        }
        if (isMoving)
        {
            Move();
        }
    }
    public void startMoving(ControlType control)
    {
        SetJellosOrder();
        controlType = control;
        isMoving = true;
        inputEnabled = true;
        WallCheck();
    }
    public float GetCurrentMass()
    {
        return currentMass;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(OBSTACLE))
        {
            currentObstacle = other.GetComponent<Obstacle>();
            EnterObstacle(other.transform);
        }
        if (other.CompareTag(FINISHLINE))
        {
            Debug.Log("Level complete");
            isMoving = false;
            getUI.LevelComplete();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(OBSTACLE))
        {
            ExitObstacle(other.transform);
        }
        
    }

    void Move()
    {
        transform.position += new Vector3(0f, 0f, moveSpeed * Time.deltaTime);
    }
    void SwipedUp()
    {
        if (currentJelloCount < maxJellos)
        {
            currentJelloCount++;
            SetJellosOrder();
        }
        WallCheck();
    }
    void SwipedDown()
    {
        if (currentJelloCount > 1)
        {
            currentJelloCount--;
            SetJellosOrder();
        }
        WallCheck();
    }

    void SetJellosOrder()
    {
        for (int i = 0; i < maxJellos; i++)
        {
            jellos[i].gameObject.SetActive(false);
        }
        SetUpNewPositions();
        for (int i = 0; i < currentJelloCount; i++)
        {
            jellos[i].gameObject.SetActive(true);
            //if (controlType == ControlType.TAP_AND_HOLD)
            if(isBouncing)
                jellos[i].SetPosition(jelloPositions[i], transitionTime, currentJelloCount, currentMass);

        }
        //if (controlType == ControlType.SWIPE)
        WallCheck();
    }
    void ResetJellos()
    {
        for (int i = 0; i < maxJellos; i++)
        {
            jellos[i].gameObject.SetActive(false);
        }
        SetUpNewPositions();
        jellos[0].gameObject.SetActive(true);
        jellos[0].SetPosition(jelloStartPos, transitionTime, currentJelloCount, currentMass);
        //if (controlType == ControlType.SWIPE)
        //WallCheck();
    }

    void SetUpNewPositions()
    {
        float[] yy = new float[currentJelloCount];
        float div = currentJelloCount / 2f;
        div *= gap;
        float xOffset = div / currentJelloCount;
        jelloPositions = new List<Vector3>();
        for (int i = 0; i < currentJelloCount; i++)
        {
            yy[i] = i * gap;
            yy[i] -= div;
            yy[i] += xOffset;
            Vector3 temp = new Vector3(yy[i], 0.5f, 0f);
            jelloPositions.Add(temp);
        }
    }

    void EnterObstacle(Transform _obstacle)
    {
        inputEnabled = false;
        int totalHoles = currentObstacle.GetNumberOfHoles();
        List<Transform> holes = currentObstacle.GetHoles();

        for (int i = 0; i < currentJelloCount; i++)
        {
            jellos[i].TransitionEffect(currentMass, 0.5f);
        }
        if (currentJelloCount == totalHoles)
        {
            // jello pass
            for (int i = 0; i < currentJelloCount; i++)
            {
                jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
            }
        }
        else
        {
            jelloLife--;
            currentMass -= currentMass * shedMassPerc;
            if (currentJelloCount < totalHoles)
            {
                //isMoving = false;
                for (int i = 0; i < currentJelloCount; i++)
                {
                    jellos[i].MassShedding(currentMass);
                }
                //getUI.LevelComplete();
            }
            else
            {
                for (int i = 0; i < currentJelloCount; i++)
                {
                    jellos[i].Splat();
                }
                currentJelloCount = totalHoles;
            }
            //-----------------
            if (currentJelloCount > totalHoles)
            {
                for (int i = 0; i < currentJelloCount; i++)
                {
                    if (i < totalHoles)
                    {
                        jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
                    }
                    else
                    {
                        jellos[i].SetPosition(jelloPositions[i], transitionTime, currentJelloCount, currentMass, true);
                    }
                }
                //Debug.Log("too many!! : " + (currentJelloCount - totalHoles));
            }
            else
            {
                for (int i = 0; i < currentJelloCount; i++)
                {
                    jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
                }
                //Debug.Log("too less!! : " + (totalHoles - currentJelloCount));
            }
        }
    }
    void ExitObstacle(Transform _obstacle)
    {
        inputEnabled = true;
        currentObstacle.RemoveObstacle();
        currentObstacle = null;
        getUI.UpdateLifeSlider(jelloLife);

        //holdCount = 0;
        //currentJelloCount = 1;
        SetJellosOrder();

        //ResetJellos();
        //if (controlType == ControlType.TAP_AND_HOLD)
        WallCheck();
    }
    
    void WallCheck()
    {
        if (isBouncing)
            return;

        int totalHoles = 0;
        List<Transform> holes = new List<Transform>();
        Obstacle obstacle;
        Ray ray = new Ray(transform.position + Vector3.up, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200f, wallLayer)) 
        {
            Debug.DrawLine(ray.origin, hit.point, Color.blue, 20f);
            obstacle = hit.transform.GetComponent<Obstacle>();
            totalHoles = obstacle.GetNumberOfHoles();
            holes = obstacle.GetHoles();
            //bool showTRansitionEffcet = false;
            //if (currentJelloCount == oldJelloCount)
            //    showTRansitionEffcet = false;
            //else
            //    showTRansitionEffcet = true;


            //for (int i = 0; i < currentJelloCount; i++)
            //{
            //    jellos[i].SetPosition(jelloPositions[i], transitionTime, currentJelloCount, currentMass);
            //}

            if (currentJelloCount == totalHoles)
            {
                // jello pass
                for (int i = 0; i < currentJelloCount; i++)
                {
                    jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
                }
            }
            else
            {
                if (currentJelloCount > totalHoles)
                {
                    for (int i = 0; i < currentJelloCount; i++)
                    {
                        if (i < totalHoles)
                        {
                            jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
                        }
                        else
                        {
                            jellos[i].SetPosition(jelloPositions[i], transitionTime, currentJelloCount, currentMass, true);
                        }
                    }
                    //Debug.Log("too many!! : " + (currentJelloCount - totalHoles));
                }
                else
                {
                    for (int i = 0; i < currentJelloCount; i++)
                    {
                        jellos[i].SetPositionForHole(holes[i], transitionTime, currentJelloCount, currentMass);
                    }
                    //Debug.Log("too less!! : " + (totalHoles - currentJelloCount));
                }
            }
        }
        else
        {
            for (int i = 0; i < currentJelloCount; i++)
            {
                jellos[i].SetPosition(jelloPositions[i], transitionTime, currentJelloCount, currentMass);
            }
        }
    }


    //void SeparateTwoJellos(int one, int two, float _transitionTime)
    //{
    //    jellos[one].gameObject.SetActive(true);
    //    jellos[two].gameObject.SetActive(true);

    //    Vector3 left = new Vector3(-0.7f, 0f, 0f);
    //    Vector3 right = new Vector3(0.7f, 0f, 0f);
    //    jellos[one].SetForMitosis(0.5f, left);
    //    jellos[two].SetForMitosis(0.5f, right, true);
    //    //StartCoroutine(SepartionRoutine(one, two, _transitionTime));
    //}
}

public enum ControlType
{
    SWIPE,
    DRAG
}
