﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    [SerializeField] GameObject holePrefab;
    [SerializeField] Vector3 holeStartPosition;
    [SerializeField] int maxNumberOfHole = 5;
    [SerializeField] Vector2 xRange;
    [SerializeField] Vector2 yRange;

    [SerializeField] List<Transform> holes;

    int totalHoles = 1;
    void Start()
    {
        holes = new List<Transform>();
        totalHoles = Random.Range(1, maxNumberOfHole);
        float[]xValues = SetNewHolePositions(totalHoles);
        for (int i = 0; i < totalHoles; i++)
        {
            GameObject gg = Instantiate(holePrefab, transform);
            float xx = holePrefab.transform.localScale.x / transform.localScale.x;
            float yy = 0.25f;
            float zz = holePrefab.transform.localScale.z / transform.localScale.y;
            float sizeOffset = 1f / totalHoles;
            Vector3 scale = new Vector3(xx +(xx * sizeOffset) , yy , zz + (zz * sizeOffset) );
            gg.transform.localPosition = new Vector3(xValues[i], Random.Range(yRange.x, yRange.y), holeStartPosition.z);
            gg.transform.localScale = scale;
            holes.Add(gg.transform);
        }
    }

    public int GetNumberOfHoles()
    {
        return totalHoles;
    }
    public List<Transform> GetHoles()
    {
        return holes;
    }
    public void RemoveObstacle()
    {
        for (int i = 0; i < totalHoles; i++)
        {
            holes[i].gameObject.SetActive(false);
        }
        transform.DOScale(Vector3.zero, 0.5f);
    }
    float[] SetNewHolePositions(int holeCount)
    {
        float gap = 0.25f;
        float[] yy = new float[holeCount];
        float div = holeCount / 2f;
        div *= gap;
        float xOffset = div / holeCount;
        for (int i = 0; i < holeCount; i++)
        {
            yy[i] = i * gap;
            yy[i] -= div;
            yy[i] += xOffset;
            //Vector3 temp = new Vector3(yy[i], 0.5f, 0f);
        }
        return yy;
    }

}
