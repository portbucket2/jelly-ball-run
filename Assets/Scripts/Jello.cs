﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Jello : MonoBehaviour
{
    public float wobbleSpeed = 10f;
    public float kickBackForce = 5f;
    public float jumpHeight = 2f;
    public float jumpDuration = 0.3f;
    public bool splatable = false;
    [SerializeField] LayerMask wallLayer;
    [SerializeField] ParticleSystem splatParticle;
    [Header("Debug")]
    [SerializeField] float currentMass = 1f;
    [SerializeField] int ownIndex = 0;
    [SerializeField] Transform ownHole;
    [SerializeField] Transform otherPartner;
    [SerializeField] bool isOnHole;
    [SerializeField] float lookAt;
    [SerializeField] bool isTracking = false;
    [SerializeField] bool isRight = false;
    [SerializeField] bool isBouncing = true;

    Vector3 nextPosition;
    float transitionTime;
    float jelloCount;
    Material jelloMat;

    float initTransitionTime = 0.33f;
    Vector3 ONE = new Vector3(2f, 1f, 2f);

    Vector3 left = new Vector3(-0.7f, 0f, 0f);
    Vector3 right = new Vector3(0.7f, 0f, 0f);

    readonly string DISTURBANCE = "_disturbance";
    readonly string SCALE = "_scale";
    readonly string POWER = "_Buldge_Power";
    

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    private void Awake()
    {
        if(transform.parent)
            isBouncing = transform.parent.GetComponent<PlayerController>().isBouncing;
    }
    void Start()
    {
        jelloMat = GetComponent<MeshRenderer>().material;
        ownIndex = transform.GetSiblingIndex() + 1;
        
    }
    private void Update()
    {
        if (isTracking)
        {
            /* mitosis part
            Vector3 dir = otherPartner.position - transform.position;
            Quaternion XLookRotation = Quaternion.LookRotation(dir, transform.up) * Quaternion.Euler(new Vector3(0, -90, 0));
            transform.rotation = XLookRotation;
            */
        }
    }
    public void Splat()
    {
        if (splatable)
        {
            //Debug.Log("splat");
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200f, wallLayer))
            {
                GameObject gg = Instantiate(splatParticle.gameObject);

                gg.transform.position = hit.point - (transform.forward * 0.1f);
                gg.GetComponent<ParticleSystem>().Play();
                //Debug.Log("splat particle!!");
            }
            NewMesh();
            gameObject.SetActive(false);
            
        }

    }
    public void MassShedding(float _currentMass)
    {
        currentMass = _currentMass;
        //Debug.Log("mass shed!!");
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200f, wallLayer))
        {
            GameObject gg = Instantiate(splatParticle.gameObject);

            gg.transform.position = hit.point - (transform.forward * 0.1f);
            gg.GetComponent<ParticleSystem>().Play();
            //Debug.Log("splat particle!!");
        }
        ShedMesh();
        //gameObject.SetActive(false);
    }
    
    public void TransitionEffect(float _currentMass, float _transitionTime)
    {
        currentMass = _currentMass;
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3((ss * 0.5f), (ss * 0.5f), ss  * 2f), _transitionTime).SetEase(Ease.OutBounce).OnComplete(TransitionEffectDone);
    }
    void TransitionEffectDone()
    {
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
    }
    public void SeparationEffect(float _currentMass, float _transitionTime)
    {
        currentMass = _currentMass;
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss * 2f, (ss * 0.5f), (ss * 0.5f)), _transitionTime).SetEase(Ease.OutBounce).OnComplete(SeparationEffectDone);
    }
    void SeparationEffectDone()
    {
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
    }
    void DoneIntialEffect()
    {
        transform.DOKill();
        //transform.DOLocalMove(nextPosition, transitionTime).SetEase(Ease.OutSine);
        transform.DOLocalMoveX(nextPosition.x, transitionTime).SetEase(Ease.OutSine);
        transform.DOLocalMoveY(nextPosition.y, transitionTime).SetEase(Ease.OutSine);
        transform.DOLocalMoveZ(0f, transitionTime).SetEase(Ease.OutSine);
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss, ss, ss + (ss* 0.25f) ), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
        //SetBuldge(transitionTime / 2f, 0f);
    }
    void DoneTransition()
    {
        Wobble();
        ResetRotation();
        isTracking = false;
        if(isBouncing)
            Bounce();
    }
    public void SetPosition(Vector3 _position, float _transitionTime, float _currentJelloCount, float _currentMass, bool _Splatable = false)
    {
        currentMass = _currentMass;
        nextPosition = _position;
        transitionTime = _transitionTime;
        jelloCount = _currentJelloCount;
        splatable = _Splatable;
        ownHole = null;


        transform.DOKill();
        transform.localScale = ONE * 0.5f;
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss * 0.5f, (ss * 0.5f), (ss * 0.5f)), transitionTime / 3f).SetEase(Ease.Flash).OnComplete(DoneIntialEffect);

        /* mitosis part
        transform.DOKill();
        if (jelloCount == 1)
        {
            //Vector3 newPos = new Vector3(nextPosition.x, nextPosition.y, transform.parent.position.z);
            //newPos = transform.InverseTransformPoint(newPos);
            //transform.localPosition = new Vector3(0f, 1.5f,0f);
            transform.DOLocalMove(nextPosition, transitionTime).SetEase(Ease.Flash);
            //transform.DOLocalMove(newPos, initTransitionTime).SetEase(Ease.Flash);
            //transform.localScale = ONE * 0.5f;
            float ss = 1.2f / jelloCount;
            transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
        }
        else if (jelloCount > 1 && ownIndex == jelloCount)
        {
            SetForMitosis(transitionTime, right, true);
        }
        else if (jelloCount > 1 && ownIndex == jelloCount - 1)
        {
            SetForMitosis(transitionTime, left);
        }
        */
        
    }
    public void SetPositionForHole(Transform hole, float _transitionTime, float _currentJelloCount, float _currentMass)
    {
        currentMass = _currentMass;
        transitionTime = _transitionTime;
        jelloCount = _currentJelloCount;
        splatable = false;
        ownHole = hole;
        transform.DOKill();
        /* mitosis 
        if (jelloCount == 1)
        {
            Vector3 _position = hole.transform.position;// transform.InverseTransformPoint(hole.position);
            Vector3 newPos = new Vector3(_position.x, _position.y, transform.parent.position.z);
            //newPos = transform.InverseTransformPoint(newPos);
            //transform.localPosition = new Vector3(0f, 1.5f,0f);
            transform.DOMove(newPos, transitionTime).SetEase(Ease.Flash);
            //transform.DOLocalMove(newPos, initTransitionTime).SetEase(Ease.Flash);
            //transform.localScale = ONE * 0.5f;
            float ss = 1.2f / jelloCount;
            transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
        }
        else if (jelloCount > 1 && ownIndex == jelloCount)
        {
            SetForMitosis(transitionTime, right, true);
        }
        else if (jelloCount > 1 && ownIndex == jelloCount - 1)
        {
            SetForMitosis(transitionTime, left);
            //}
        */

        Vector3 _position = hole.transform.position;// transform.InverseTransformPoint(hole.position);
        Vector3 newPos = new Vector3(_position.x, _position.y, transform.parent.position.z);
        //newPos = transform.InverseTransformPoint(newPos);
        //transform.DOMove(newPos, transitionTime).SetEase(Ease.Flash);
        transform.DOMoveX(newPos.x, transitionTime).SetEase(Ease.Flash);
        transform.DOMoveY(newPos.y, transitionTime).SetEase(Ease.Flash);
        transform.DOLocalMoveZ(0f, transitionTime).SetEase(Ease.Flash);
        //transform.DOLocalMove(newPos, initTransitionTime).SetEase(Ease.Flash);
        //transform.localScale = ONE * 0.5f;
        //float ss = 1.2f / jelloCount;
        //transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
        TransitionEffect(currentMass, transitionTime);
    }
    void OnHole()
    {
        if (ownHole)
        {
            transform.DOKill();
            Vector3 _position = ownHole.transform.position;// transform.InverseTransformPoint(hole.position);
            Vector3 newPos = new Vector3(_position.x, _position.y, transform.parent.position.z);
            //newPos = transform.InverseTransformPoint(newPos);
            //transform.localPosition = new Vector3(0f, 1.5f,0f);
            transform.DOMove(newPos, transitionTime).SetEase(Ease.InSine);
            //transform.DOLocalMove(newPos, initTransitionTime).SetEase(Ease.Flash);
            //transform.localScale = ONE * 0.5f;
            float ss = 1.2f / jelloCount;
            transform.DOScale(new Vector3(ss, ss, ss + (ss * 0.25f)), transitionTime).SetEase(Ease.OutBounce).OnComplete(DoneTransition);
        }
        else
        {
            DoneIntialEffect();
        }
    }
    void Wobble()
    {
        //WallCheck();
        if (gameObject.activeInHierarchy)
        {
            StopAllCoroutines();
            StartCoroutine(WobbleRoutine());
        }
        //StartCoroutine(WobbleRoutineScale());
    }
    IEnumerator WobbleRoutine()
    {
        float dis = 0;
        //while (dis < 0.25f)
        //{
        //    jelloMat.SetFloat(DISTURBANCE, dis);
        //    dis += Time.deltaTime;
        //    yield return ENDOFFRAME;
        //}
        dis = 0.25f;
        yield return ENDOFFRAME;
        while (dis > 0.02f)
        {
            jelloMat.SetFloat(DISTURBANCE, dis);
            dis -= Time.deltaTime;
            yield return ENDOFFRAME;
        }
        jelloMat.SetFloat(DISTURBANCE, 0.001f);
    }
    IEnumerator WobbleRoutineScale()
    {
        float dis = 5f;
        while (dis > 1f)
        {
            jelloMat.SetFloat(SCALE, dis);
            dis -= Time.deltaTime * wobbleSpeed;
            yield return ENDOFFRAME;
        }
        jelloMat.SetFloat(SCALE, 1);
    }
    void NewMesh()
    {
        //GameObject obj = new GameObject("obj");
        //obj.AddComponent<MeshRenderer>().sharedMaterials = GetComponent<MeshRenderer>().sharedMaterials;
        //obj.AddComponent<MeshFilter>().sharedMesh = Instantiate(GetComponent<MeshFilter>().sharedMesh);
        GameObject gg = Instantiate(gameObject);
        gg.transform.position = transform.position;
        gg.transform.localScale = transform.localScale;

        gg.AddComponent<Rigidbody>();
        gg.AddComponent<SphereCollider>();
        gg.GetComponent<Rigidbody>().AddForce(-Vector3.forward * kickBackForce, ForceMode.Impulse);
        //Debug.Log("new mesh tried :" + gg);
    }
    void ShedMesh()
    {
        //GameObject obj = new GameObject("obj");
        //obj.AddComponent<MeshRenderer>().sharedMaterials = GetComponent<MeshRenderer>().sharedMaterials;
        //obj.AddComponent<MeshFilter>().sharedMesh = Instantiate(GetComponent<MeshFilter>().sharedMesh);
        GameObject gg = Instantiate(gameObject);
        gg.transform.position = transform.position;
        gg.transform.localScale = transform.localScale * 0.25f;

        gg.AddComponent<Rigidbody>();
        gg.AddComponent<SphereCollider>();
        gg.GetComponent<Rigidbody>().AddForce(-Vector3.forward * kickBackForce, ForceMode.Impulse);
        //Debug.Log("new mesh tried :" + gg);
    }

    
    public void SetForMitosis(float time, Vector3 _newPos, bool _isRight = false)
    {
        isRight = _isRight;
        jelloMat.DOKill();
        transform.DOKill();
        int index = 0;
        transform.localScale = Vector3.one;
        if (_isRight)
        {
            transform.localEulerAngles = new Vector3(0f, 200f, 0f);
            SetScaleOnly(time);
            index = ownIndex-2;
            otherPartner = transform.parent.GetChild(transform.GetSiblingIndex() - 1);
        }
        else
        {
            transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            index = ownIndex-1;
            otherPartner = transform.parent.GetChild(transform.GetSiblingIndex() + 1);
        }
        transform.localPosition = new Vector3(0f, 1.5f, 0f);
        Vector3 newPos = new Vector3(_newPos.x, 1.5f, _newPos.z);
        //jelloMat.SetFloat(POWER, 1.2f);
        //SetBuldge(time / 4f, 1.2f);
        transform.DOLocalMove(newPos, time).SetEase(Ease.OutSine).OnComplete(() => WobbleAndHole());
        isTracking = true;
    }
    void SetScaleOnly(float time )
    {
        float ss = 1.2f / jelloCount;
        transform.localScale = Vector3.one * 0.1f;
        transform.DOScale(new Vector3(1f, 1f, 1f + (1f * 0.25f)), time).SetEase(Ease.Linear);
    }
    //void SetBuldge(float time, float endValue)
    //{

    //    jelloMat.DOKill();
    //    //jelloMat.DOFloat(endValue, POWER, time).SetEase(Ease.Flash).OnComplete(()=> WobbleAndHole()) ;
    //    WobbleAndHole();
    //}
    void WobbleAndHole()
    {
        Wobble();
        OnHole();        
    }
    void ResetRotation()
    {
        transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }
    void Bounce()
    {
        float per = jumpDuration * 0.5f;
        float time = jumpDuration + Random.Range(jumpDuration - per, jumpDuration + per);
        //Vector3 jump = new Vector3(0f, jumpHeight, 0f);
        transform.DOLocalMoveY(jumpHeight, time).SetEase(Ease.OutSine).OnComplete(BounceEnd);
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss, ss + (ss * 0.5f), ss ), time).SetEase(Ease.OutSine);

        //transform.DOPunchPosition(jump,jumpDuration,1,0.8f,true);
    }
    void BounceEnd()
    {
        float per = jumpDuration * 0.5f;
        float time = jumpDuration + Random.Range(jumpDuration - per, jumpDuration + per);
        transform.DOLocalMoveY(0, time).SetEase(Ease.InSine).OnComplete(Bounce);
        
        float ss = currentMass / jelloCount;
        transform.DOScale(new Vector3(ss + (ss * 0.75f), ss, ss ), time).SetEase(Ease.InSine);

        //transform.DOScaleX(ss * 2f, jumpDuration);
    }
}
