﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpwaner : MonoBehaviour
{
    [SerializeField] GameObject obstaclePrefab;
    [SerializeField] Transform finishLine;
    [SerializeField] int totalSpwans = 5;
    [SerializeField] float gap = 10f;
    [SerializeField] Vector2 randomMinMax;

    Vector3 startPosition;

    void Start()
    {
        startPosition = obstaclePrefab.transform.position;
        for (int i = 1; i < totalSpwans + 1; i++)
        {
            GameObject gg = Instantiate(obstaclePrefab,transform);
            gg.transform.position = new Vector3(0f, startPosition.y, i * gap + Random.Range(randomMinMax.x, randomMinMax.y));
        }
        float finishPos = ((totalSpwans + 2) * gap) - gap / 2f;
        finishLine.position = new Vector3(0f, -0.5f, finishPos);
    }
}
