﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    [SerializeField] Button resetButton;
    [SerializeField] GameObject LevelEndPanel;
    [SerializeField] Image lifeSlider;
    [SerializeField] GameObject controlSelectPanel;
    [SerializeField] Button swipeButton;
    [SerializeField] Button dragButton;
    [SerializeField] PlayerController playerController;

    float max = 0;
    void Start()
    {
        resetButton.onClick.AddListener(delegate {
            SceneManager.LoadScene(0);
	    });
        swipeButton.onClick.AddListener(delegate {
            controlSelectPanel.SetActive(false);
            playerController.startMoving(ControlType.SWIPE);
        });
        dragButton.onClick.AddListener(delegate {
            controlSelectPanel.SetActive(false);
            playerController.startMoving(ControlType.DRAG);
        });
    }

    // Update is called once per frame
    public void LevelComplete()
    {
        LevelEndPanel.SetActive(true);
    }
    public void SetLifeSlider(float _max)
    {
        max = _max;
    }
    public void UpdateLifeSlider(float val)
    {
        float endV = val / max;
        lifeSlider.DOFillAmount(endV, 0.05f);
    }
}
