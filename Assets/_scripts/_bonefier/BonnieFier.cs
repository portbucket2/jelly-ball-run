﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonnieFier : MonoBehaviour
{

    public bool isCalculating = false;
    public bool previousPosition = false;

    
    
    public float radius;
    public float radius2;
    public float skinOffset;

    public Transform bone;
    //private FakeBoneObject boneObject;
    //private DermalPiercingSlot piercingSlot;

    private float step = 0f;
    private MeshFilter meshFilter;
    private Mesh mesh;
    private bool isSelectMode = true;

    BonnieVertex[] bonnieVertices;
    Vector3[] currentMeshVertices;
    //Vector3 hitPoint;
    List<int> selectedVertices;
    List<int> selectedVertices2;
    //SceneController sceneController;
    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;
        //GetVertices();
        //sceneController = SceneController.GetController();
        StartBonnifing();
    }
    

    // Update is called once per frame
    void Update()
    {
        if (isCalculating)
        {
            UpdateVertices();
            step = 0;
        }

        if (previousPosition)
        {
            isCalculating = false;
            if (step < 1) 
            {
                ResetAllVertex(step);
                step += Time.deltaTime;
            }
            else
            {
                previousPosition = false;
                step = 0;
            }
        }
    }
    public void SetBonnifier(Transform _bone)
    {
        //boneObject = _bone;
        bone = _bone;
        //piercingSlot = boneObject.GetPiercingSlot();
    }
    public void StartBonnifing()
    {
        //bone.position = hit.point - hit.normal * skinOffset;
        //bone.LookAt(bone.position - hit.normal * 2f);

        //no need to call here call when selected for this position
        GetVertices();
        SelectCloseVertices(bone.localPosition);
        isCalculating = true;

        isSelectMode = false;
        //StartPiercing(ring);
    }
    //void StartPiercing(RingHolder _ring)
    //{
    //    piercingSlot.PiercingStart(this, _ring);
    //}
    void SelectCloseVertices(Vector3 point)
    {
        selectedVertices = new List<int>();
        selectedVertices2 = new List<int>();
        int max = mesh.vertices.Length;
        Vector3 pointy = transform.InverseTransformPoint(point);
        for (int i = 0; i < max; i++)
        {
            if (Vector3.SqrMagnitude(pointy - currentMeshVertices[i])< radius* radius)
            {
                selectedVertices.Add(i);
            }
            if (Vector3.SqrMagnitude(pointy - currentMeshVertices[i]) >= radius*radius && Vector3.SqrMagnitude(pointy - currentMeshVertices[i]) < radius2*radius2)
            {
                selectedVertices2.Add(i);
            }
        }
        //Debug.Log("vertices count: " + selectedVertices.Count);
        //Debug.Log("vertices2 count: " + selectedVertices2.Count);
    }

    public void GetVertices()
    {
        int max = mesh.vertices.Length;
        bonnieVertices = new BonnieVertex[max];
        currentMeshVertices = mesh.vertices; 
        for (int i = 0; i < max; i++)
        {
            bonnieVertices[i] = new BonnieVertex(i, currentMeshVertices[i], currentMeshVertices[i], bone.position);
        }
        //Debug.Log("total vertex count: " + bonnieVertices.Length);
    }

    private void UpdateVertices()
    {
        int max = bonnieVertices.Length;
        int min = selectedVertices.Count;
        int miner = selectedVertices2.Count;
        for (int i = 0; i < min; i++)
        {
            //Debug.Log("bone position: " + bone.position);
            bonnieVertices[selectedVertices[i]].TranslateVertex(transform, bone.position,1f);
        }
        for (int i = 0; i < miner; i++)
        {
            //Debug.Log("bone position: " + bone.position);
            bonnieVertices[selectedVertices2[i]].TranslateVertex(transform, bone.position, 4f);
        }

        for (int i = 0; i < max; i++)
        {
            currentMeshVertices[i] = bonnieVertices[i].currentVertextPosition;
        }

        mesh.vertices = currentMeshVertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }
    private void ResetAllVertex(float step)
    {
        
        int max = bonnieVertices.Length;
        for (int i = 0; i < max; i++)
        {
            bonnieVertices[i].ResetVertex();
            currentMeshVertices[i] = Vector3.Lerp(currentMeshVertices[i], bonnieVertices[i].currentVertextPosition, step);
        }

        mesh.vertices = currentMeshVertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }
    public void Reset()
    {
        isSelectMode = true;
        previousPosition = true;
    }
    //public FakeBoneObject GetBoneObject() { return boneObject; }

}
