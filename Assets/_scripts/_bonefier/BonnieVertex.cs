﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonnieVertex 
{
    public int vertexIndex;
    public Vector3 initialVertextPosition;
    public Vector3 currentVertextPosition;
    public Vector3 bonePosition;

    public BonnieVertex(int _vertexIndex, Vector3 _initialVertextPosition, Vector3 _currentVertextPosition, Vector3 _bonePosition)
    {
        vertexIndex = _vertexIndex;
        initialVertextPosition = _initialVertextPosition;
        currentVertextPosition = _currentVertextPosition;
        bonePosition = _bonePosition;
    }
    public void TranslateVertex(Transform _transform, Vector3 _position,float div)
    {
        float diff = Vector3.Distance(currentVertextPosition, _transform.InverseTransformPoint(_position));
        Vector3 distanceVerticePoint = bonePosition - _position; //currentVertextPosition - _transform.InverseTransformPoint(_position);
        Vector3 temp = new Vector3(distanceVerticePoint.x, -distanceVerticePoint.y, distanceVerticePoint.z);
        //currentVertextPosition += temp / (div + diff);
        currentVertextPosition += temp / (1 / 1 + (diff * 10f));
        //if (div > 1)
        //{
        //    currentVertextPosition += temp / (div + diff);
        //}
        //else
        //    currentVertextPosition += temp * (1 / 1 + diff);

        bonePosition = _position;
    }
    public void ResetVertex()
    {
        currentVertextPosition = initialVertextPosition;
    }
    public Vector3 GetCurrentVertexPosition()
    {
        return currentVertextPosition;
    }
    public int GetVertexIndex()
    {
        return vertexIndex;
    }
}
